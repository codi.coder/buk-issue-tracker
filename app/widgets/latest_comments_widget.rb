class LatestCommentsWidget < BaseWidget

  def comments
    IssueComment.order(created_at: :desc)
        .limit(10)
        .includes(:issue)
  end

end

