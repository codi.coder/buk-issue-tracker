module ApplicationHelper

  def user_gravatar_url(user)
    "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(user.email.downcase)}?s=64&d=identicon"
  end

end
