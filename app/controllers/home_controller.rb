class HomeController < ApplicationController
  def index

    @widgets = [
        [::LatestCommentsWidget.new],
        [::AssignedIssuesWidget.new(current_user), LatestIssuesWidget.new],
    ]

  end
end
