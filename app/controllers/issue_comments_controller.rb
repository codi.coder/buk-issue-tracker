class IssueCommentsController < ApplicationController
  before_action :set_issue
  before_action :set_issue_comment, only: [:update, :destroy]

  # POST /issues/:issue_id/comments
  # POST /issues/:issue_id/comments.json
  def create
    @issue_comment = IssueComment.new(issue_comment_params)
    @issue_comment.issue = @issue
    @issue_comment.user = current_user

    respond_to do |format|
      if @issue_comment.save
        format.html { redirect_to @issue, notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @issue_comment }
      else
        flash[:error] = @issue_comment.errors

        format.html { redirect_to @issue, error: @issue_comment.errors }
        format.json { render json: @issue_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /issues/:issue_id/comments/1
  # PATCH/PUT /issues/:issue_id/comments/1.json
  def update
    respond_to do |format|
      if @issue_comment.update(issue_comment_params)
        format.html { redirect_to @issue, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @issue_comment }
      else
        format.html { redirect_to @issue, error: @issue_comment.errors }
        format.json { render json: @issue_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /issues/:issue_id/comments/1
  # DELETE /issues/:issue_id/comments/1.json
  def destroy
    @issue_comment.destroy
    respond_to do |format|
      format.html { redirect_to @issue, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_issue
      begin
        @issue = Issue.find(params[:issue_id])
      rescue ActiveRecord::RecordNotFound
        flash[:error] = "Issue not found"
        redirect_back(fallback_location: issues_path)
      end
    end

    def set_issue_comment
      begin
        @issue = IssueComment.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:error] = "Comment not found"
        redirect_back(fallback_location: issue_path(@issue))
      end
    end

    def issue_comment_params
      params.require(:issue_comment).permit(:comment)
    end

end
