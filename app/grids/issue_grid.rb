class IssueGrid

  attr_reader :issues_query, :page, :last_page, :issues_total

  def initialize
    @issues_query = Issue.joins(:issue_state, :issue_type, :issue_priority)
    @page = 1
  end

  def set_page(page)
    @page = page.to_i

    @issues_total = @issues_query.count
    @last_page = (@issues_total/20.0).ceil

    if @page < 1
      raise ActionController::ActionControllerError.new("Page cannot be less than 1!")
    end
  end

  def in_project(project)
    @issues_query = @issues_query.where('issues.project_id = ?', project)
  end

  def search_for(search)
    @issues_query = @issues_query.where('issues.name LIKE ?', "%#{search}%")
  end

  def get_results
    @issues_query
        .order(:completed)
        .order(updated_at: :desc)
        .limit(20)
        .offset((@page-1) * 20)
        .includes(:issue_state, :issue_type, :issue_priority)
        .all
  end

  def is_page?(p)
    @page == p
  end

  def has_previous?
    @page > 1
  end

  def has_next?
    @page < @last_page
  end

end
