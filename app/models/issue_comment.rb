class IssueComment < ApplicationRecord

  belongs_to :user
  belongs_to :issue

  validates :comment, presence: true

  after_save :update_issue_timestamps

  private
  def update_issue_timestamps
    self.issue.update(updated_at: DateTime.now)
  end

end
