class Issue < ApplicationRecord

  belongs_to :issue_type
  belongs_to :issue_priority
  belongs_to :issue_state
  belongs_to :project

  belongs_to :assigned_user, class_name: "User", :required => false

  has_many :issue_comments, dependent: :destroy

  validates :name, :issue_type_id, :issue_priority_id, :issue_state_id, :project_id, presence: true

  before_save :create_issue_key

  def is_assigned?
    !!assigned_user
  end

  private
  def create_issue_key
    self.issue_key = self.project.create_key_for_new_issue unless self.issue_key
  end

end
