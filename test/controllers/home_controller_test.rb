require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "should get redirected to login" do
    get root_url
    assert_redirected_to new_user_session_url
  end

  test "should get index" do
    sign_in users(:user1)
    get root_url
    assert_response :success
  end

end
