require 'test_helper'

class IssueTest < ActiveSupport::TestCase

  test "creating issue generates issue key" do
    project = Project.create!(name: 'Test project', project_key: 'TEST', next_issue_key: 1)
    issue_type = issue_types(:type_feature)
    issue_state = issue_states(:state_new)
    issue_priority = issue_priorities(:priority_normal)
    issue = Issue.create!(name: 'First issue', description: 'Lorem ipsum', project: project, issue_type: issue_type, issue_state: issue_state, issue_priority: issue_priority)

    assert_equal("TEST-1", issue.issue_key)
    assert_equal(2, project.next_issue_key)
  end

end
