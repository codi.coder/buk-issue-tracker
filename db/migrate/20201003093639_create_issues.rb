class CreateIssues < ActiveRecord::Migration[6.0]
  def change
    create_table :issue_types do |t|
      t.string :name
      t.string :icon
      t.string :color

      t.index :name
    end

    create_table :issue_priorities do |t|
      t.string :name
      t.string :icon
      t.string :color
      t.integer :value, default: 5

      t.index :name
      t.index :value
    end

    create_table :issue_states do |t|
      t.string :name
      t.string :color
      t.integer :completed, default: 0

      t.index :name
    end

    create_table :issues do |t|
      t.string :name
      t.text :description

      t.references :issue_type, null: false, foreign_key: true
      t.references :issue_priority, null: false, foreign_key: true
      t.references :issue_state, null: false, foreign_key: true
      t.references :project, null: false, foreign_key: true

      t.timestamps
    end
  end
end
